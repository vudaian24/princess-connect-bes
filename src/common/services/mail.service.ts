import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailService {
    constructor(private readonly mailerService: MailerService) { }

    async sendActivationEmail(email: string, activationCode: string) {
        const activationLink = `https://yourapp.com/activate/${activationCode}`;
        await this.mailerService.sendMail({
            to: email,
            subject: 'Activate your account',
            text: `Please click the following link to activate your account: ${activationLink}`,
        });
    }
}
