import { Comment, CommentDocument } from "../../database/schemas/comment.schema";
import { BaseRepository } from "../../common/base/base.repository";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Injectable } from "@nestjs/common";

@Injectable()
export class CommentRepository extends BaseRepository<Comment> {
    constructor(
        @InjectModel(Comment.name)
        private readonly commentModel: Model<CommentDocument>
    ) {
        super(commentModel);
    }

    async createOneComment(
        userId: string, 
        conntent: string
    ): Promise<Comment | null> {
        try {
            const comment = new this.commentModel({
                user_id: userId,
                content: conntent,
            });
            await comment.save();
            return comment;
        } catch (error) {
            this.logger.error('Error in PostRepository createOneComment: ' + error);
            throw error;
        }
    }
    async findAllByQuery(condition: Partial<Comment>) {
        try {
            const comments = await this.commentModel.find(condition); 
            return {
                totalItems: comments.length,
                items: comments || []
            };
        } catch (error) {
            this.logger.error('Error in PostRepository find: ' + error);
            throw error;
        }
    }
}