import { Friendship } from "../../database/schemas/friendship.schema";

export enum FriendshipOrderBy {
    ID = 'id',
    USER_ID_SEND = 'user_id_send',
    USER_ID_RECEIVE = 'user_id_receive',
    STATUS = 'status',
    CREATED_AT = 'createdAt',
    UPDATED_AT = 'updatedAt',
}

export const FriendshipAttributesForList: (keyof Friendship)[] = [
    '_id',
    'id',
    'user_id_send',
    'user_id_receive',
    'status',
    'createdAt',
    'updatedAt',
];

export const FriendshipAttributesForDetail: (keyof Friendship)[] = [
    'id',
    'user_id_send',
    'user_id_receive',
    'status',
    'createdAt',
];