import { BaseService } from '../../../common/base/base.service';
import { HttpException, Injectable } from '@nestjs/common';
import { Types } from 'mongoose';
import {
    CreateFriendshipDto,
    UpdateFriendshipDto,
    GetFriendshipListQuery,
} from './../friend.interface';
import { Friendship, FriendshipDocument } from '../../../database/schemas/friendship.schema';
import { FriendRepository } from '../friend.repository';
import { HttpStatus } from '../../../common/constants';
import { FriendshipAttributesForDetail } from '../friend.constant';
import { UserAttributesForDetail } from '../../../modules/user/user.constant';
import { User } from '../../../database/schemas/user.schema';
import { toObjectId } from './../../../common/helpers/commonFunctions';
import { UserRepository } from './../../user/user.repository';
import { GetUserListQuery } from './../../user/user.interface';

@Injectable()
export class FriendService extends BaseService<Friendship, FriendRepository> {
    constructor(
        private readonly friendshipRepository: FriendRepository,
        private readonly userRepository: UserRepository
    ) {
        super(friendshipRepository);
    }

    async getFriendshipListByUserId(
        userId: string,
        attributes: (keyof User)[] = UserAttributesForDetail,
    ) {
        try {
            if (userId == "") {
                throw new HttpException('User not found', HttpStatus.OK);
            }
            const user = await this.userRepository.getOneById(userId, attributes);
            if (!user) {
                throw new HttpException('User not found', HttpStatus.OK);
            }

            const friendsId = user.friends;
            if (friendsId.length === 0) {
                throw new HttpException('User not found', HttpStatus.OK);
            }
            const friends = await Promise.all(
                friendsId.map((friend) => this.userRepository.findOneBy({ _id: toObjectId(friend) }))
            );
            if (friends.length === 0) {
                throw new HttpException('User not found', HttpStatus.OK);
            }
            const friendsList = friends.map((friend, index) => ({
                friend_ship_id: friendsId[index],
                user_id: friend.id,
                email: friend.email,
                username: friend.username,
                avatar: friend.avatar,
                birthday: friend.birthday,
                coverAvatar: friend.coverAvatar,
            }));
            return {
                totalItems: friendsList.length,
                items: friendsList || [],
            };
        } catch (error) {
            this.logger.error('Error in FriendshipService getFriendshipListByUserId: ' + error);
            throw error;
        }
    }

    async createFriendship(dto: CreateFriendshipDto) {
        try {
            const checkFriendShipSend = await this.friendshipRepository.findAllByQuery({
                user_id_send: dto.user_id_send,
                user_id_receive: dto.user_id_receive,
            })
            const checkFriendShipReceive = await this.friendshipRepository.findAllByQuery({
                user_id_send: dto.user_id_receive,
                user_id_receive: dto.user_id_send,
            })
            if (checkFriendShipSend.totalItems !== 0 || checkFriendShipReceive.totalItems !== 0) {
                return {
                    messege: "Đang chờ người dùng chấp nhận",
                    code: HttpStatus.BAD_REQUEST,
                }
            }
            const friendship: SchemaCreateDocument<Friendship> = {
                user_id_send: dto.user_id_send,
                user_id_receive: dto.user_id_receive,
                status: dto.status,
            };

            const user = await this.userRepository.findOneBy({ _id: toObjectId(dto.user_id_send) })
            const userCheck = user.friends.filter(friend => friend === dto.user_id_receive);
            if (userCheck.length > 0) {
                return {
                    messege: "Đã là bạn bè của nhau",
                    code: HttpStatus.BAD_REQUEST,
                }
            }

            const createdFriendship = await this.friendshipRepository.createOne(friendship);

            return createdFriendship;
        } catch (error) {
            this.logger.error('Error in FriendshipService createFriendship: ' + error);
            throw error;
        }
    }

    async acceptFriendship(
        dto: { user_id_send: any, user_id_receive: any }
    ) {
        try {
            const friendship = await this.friendshipRepository.findOneBy({
                user_id_send: dto.user_id_send,
                user_id_receive: dto.user_id_receive,
                status: 'pending'
            });

            if (friendship === null) {
                throw new HttpException('Friendship not found', HttpStatus.BAD_REQUEST);
            }
            await this.friendshipRepository.deleteFriendShip(toObjectId(friendship.id));
            await this.userRepository.updateOneById(
                dto.user_id_send,
                { $push: { friends: dto.user_id_receive } }
            );
            await this.userRepository.updateOneById(
                dto.user_id_receive,
                { $push: { friends: dto.user_id_send } }
            );
            return friendship;
        } catch (error) {
            this.logger.error('Error in FriendshipService acceptFriendship: ' + error);
            throw error;
        }
    }

    async updateFriendship(id: Types.ObjectId, dto: UpdateFriendshipDto) {
        try {
            const updatedFriendship = await this.friendshipRepository.updateOneById(id, dto);
            return updatedFriendship;
        } catch (error) {
            this.logger.error('Error in FriendshipService updateFriendship: ' + error);
            throw error;
        }
    }

    async deleteFriendship(id: Types.ObjectId) {
        try {
            await this.friendshipRepository.deleteFriendShip(id);
            return { id };
        } catch (error) {
            this.logger.error('Error in FriendshipService deleteFriendship: ' + error);
            throw error;
        }
    }

    async findFriendshipById(
        id: Types.ObjectId,
        attributes: (keyof Friendship)[] = FriendshipAttributesForDetail
    ) {
        try {
            const res = await this.friendshipRepository.getOneById(id, attributes);
            return res;
        } catch (error) {
            this.logger.error('Error in FriendshipService findFriendshipById: ' + error);
            throw error;
        }
    }

    async getFriendshipById(
        id: Types.ObjectId,
    ) {
        try {
            const user = await this.userRepository.findOneBy({ _id: id });
            if (!user) {
                throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
            }
            const res = await this.friendshipRepository.findAllByQuery({ user_id_receive: id.toString() });
            if (!res) {
                throw new HttpException('Friendship not found', HttpStatus.BAD_REQUEST);
            }
            const friendsId = res.items.map((item) => item.user_id_send);
            if(friendsId.length === 0) {
                return {
                    items: [],
                    totalmem: 0
                }
            }
            const friends = await Promise.all(
                friendsId.map((friend) => this.userRepository.findOneBy({ _id: toObjectId(friend), deletedAt: null }))
            );
            
            const friendsListSend = friends.map((friend, index) => ({
                friend_ship_id: res.items[index]._id,
                user_id: friend.id,
                email: friend.email,
                username: friend.username,
                avatar: friend.avatar,
                birthday: friend.birthday,
                coverAvatar: friend.coverAvatar,
            }));
            return {
                items: friendsListSend || [],
                totalmem: friendsListSend.length || 0
            }
        } catch (error) {
            this.logger.error('Error in FriendshipService getFriendshipById: ' + error);
            throw error;
        }
    }

    async unfriend(user_id_1: string, user_id_2: string) {
        try {
            const user_1 = await this.userRepository.findOneBy({ _id: toObjectId(user_id_1) });
            const user_2 = await this.userRepository.findOneBy({ _id: toObjectId(user_id_2) });

            if (!user_1 || !user_2) {
                throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
            }
            const checkFriendShipSend = await this.friendshipRepository.findAllByQuery({
                user_id_send: user_id_1,
                user_id_receive: user_id_2,
            })
            const checkFriendShipReceive = await this.friendshipRepository.findAllByQuery({
                user_id_send: user_id_2,
                user_id_receive: user_id_1,
            })
            if (!checkFriendShipSend || !checkFriendShipReceive) {
                throw new HttpException('Friendship not found', HttpStatus.BAD_REQUEST);
            }
            await this.userRepository.updateOneById(user_id_1, { $pull: { friends: user_id_2 } });
            await this.userRepository.updateOneById(user_id_2, { $pull: { friends: user_id_1 } });
            return null;
        } catch (error) {
            this.logger.error('Error in FriendshipService unfriend: ' + error);
            throw error;
        }
    }

    async getFriendshipSuggestions(user_id: string) {
        try {
            const user = await this.userRepository.findOneBy({ _id: toObjectId(user_id)});
            if (user === null) {
                throw new HttpException('Không tìm thấy người dùng này', HttpStatus.BAD_REQUEST);
            }
            const myUser = {
                user_id: user.id,
                email: user.email,
                username: user.username,
                avatar: user.avatar,
                birthday: user.birthday,
                coverAvatar: user.coverAvatar,
            }
            const friendsId = user.friends;
            const friends = await Promise.all(
                friendsId.map((friend) => this.userRepository.findOneBy({ 
                    _id: toObjectId(friend),
                }))
            );
            const friendsListSend = friends.map((friend, index) => ({
                user_id: friend.id,
                email: friend.email,
                username: friend.username,
                avatar: friend.avatar,
                birthday: friend.birthday,
                coverAvatar: friend.coverAvatar,
            }));
            const query: GetUserListQuery = {
                limit: 100,
                page: 1,
                username: '',
                role: 'user'
            }

            const users = (await this.userRepository.findAllAndCountUserByQuery(query)).items;

            const userSuggestions = users.map(user => {
                return {
                    user_id: user.id,
                    email: user.email,
                    username: user.username,
                    avatar: user.avatar,
                    birthday: user.birthday,
                    coverAvatar: user.coverAvatar,
                }
            })
            const friendsIdSet = new Set(friendsListSend.map(friend => friend.user_id));
            friendsIdSet.add(myUser.user_id);
            const filteredUserSuggestions = userSuggestions.filter(
                user => !friendsIdSet.has(user.user_id)
            );

            return {
                items: filteredUserSuggestions || [],
                totalmem: filteredUserSuggestions.length || 0
            }
        } catch (error) {
            this.logger.error('Error in FriendshipService getFriendshipSuggestions: ' + error);
            throw error;
        }
    }
}
