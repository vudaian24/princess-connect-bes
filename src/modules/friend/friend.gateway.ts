import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { FriendService } from './services/friend.service';
import { SuccessResponse } from '../../common/helpers/response';
import { toObjectId } from '../../common/helpers/commonFunctions';
@WebSocketGateway(8001, { cors: { origin: '*' } })
export class FriendGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;
  afterInit(server: Server) {
    console.log('WebSocket Gateway initialized');
  }
  constructor(private readonly friendService: FriendService) { }

  handleConnection(client: Socket, ...args: any[]) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }
  @SubscribeMessage('getListFriendRequest')
  async getFriendshipDetail(client: any, payload: { user_id: string }): Promise<void> {
    const result = await this.friendService.getFriendshipById(toObjectId(payload.user_id));
    client.emit(`${payload.user_id}_friendRequest`, new SuccessResponse(result));
  }
}
