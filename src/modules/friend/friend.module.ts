import { Module } from '@nestjs/common';
import { FriendController } from './controllers/friend.controller';
import { FriendService } from './services/friend.service';
import { UserRepository } from '../user/user.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { Friendship, FriendshipSchema } from '../../database/schemas/friendship.schema';
import { FriendRepository } from './friend.repository';
import { UserModule } from '../user/user.module';
import { FriendGateway } from './friend.gateway';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Friendship.name, schema: FriendshipSchema }]),
    UserModule
  ],
  controllers: [FriendController],
  providers: [FriendService, FriendRepository, FriendGateway]
})
export class FriendModule {}
