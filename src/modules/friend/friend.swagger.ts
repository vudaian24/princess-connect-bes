export const createFriendshipSuccessResponseExample = {
    createdAt: '2024-01-10T10:46:42.037Z',
    updatedAt: '2024-01-10T10:46:42.037Z',
    deletedAt: null,
    deletedBy: null,
    updatedBy: null,
    createdBy: null,
    user_id: 'Friend user 1',
    friend_id: 'Friend user 2',
    status: 'pending',
    _id: '659e7592b3b56d0946b3c7b6',
    __v: 0,
    id: '659e7592b3b56d0946b3c7b6',
};

export const updateFriendSuccessResponseExample = {
    _id: '659e7592b3b56d0946b3c7b6',
    user_id: 'updated user_id',
    friend_id: 'updated friend_id',
    status: 'accepted',
    id: '659e7592b3b56d0946b3c7b6',
};

export const deleteFriendshipSuccessResponseExample = {
    id: '659e7592b3b56d0946b3c7b6',
};

export const getFriendshipDetailSuccessResponseExample = {
    _id: '659e7592b3b56d0946b3c7b6',
    user_id: 'user_id detail',
    friend_id: 'friend_id detail',
    status: 'accepted',
    id: '659e7592b3b56d0946b3c7b6',
};

export const getFriendshipListSuccessResponseExample = {
    totalItems: 1,
    items: [
        {
            _id: '659e7592b3b56d0946b3c7b6',
            createdAt: '2024-01-10T10:46:42.037Z',
            updatedAt: '2024-01-10T10:47:59.566Z',
            user_id: 'user_id list',
            friend_id: 'friend_id list',
            status: 'accepted',
            id: '659e7592b3b56d0946b3c7b6',
        },
    ],
};
