import { BaseRepository } from '../../common/base/base.repository';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { GetFriendshipListQuery } from './friend.interface';
import {
    DEFAULT_FIRST_PAGE,
    DEFAULT_LIMIT_FOR_PAGINATION,
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIRECTION,
    OrderDirection,
    softDeleteCondition,
} from '../../common/constants';
import { parseMongoProjection } from '../../common/helpers/commonFunctions';
import { Friendship, FriendshipDocument } from '../../database/schemas/friendship.schema';
import { FriendshipAttributesForList } from './friend.constant';

@Injectable()
export class FriendRepository extends BaseRepository<Friendship> {
    constructor(
        @InjectModel(Friendship.name)
        private readonly friendshipModel: Model<FriendshipDocument>,
    ) {
        super(friendshipModel);
    }

    async findOneBy(condition: Partial<Friendship>): Promise<Friendship | null> {
        try {
            const friendship = await this.friendshipModel.findOne(condition);
            return friendship || null;
        } catch (error) {
            this.logger.error('Error in FriendshipRepository findOneBy: ' + error);
            throw error;
        }
    }

    async findAllByQuery(condition: Partial<Friendship>) {
        try {
            const friendships = await this.friendshipModel.find(condition);
            if(!friendships) {
                return {
                    totalItems: 0,
                    items: []
                }
            }
            return {
                totalItems: friendships.length,
                items: friendships || []
            };
        } catch (error) {
            this.logger.error('Error in FriendshipRepository findAllByQuery: ' + error);
            throw error;
        }
    }

    async deleteFriendShip(id: Types.ObjectId) {
        try {
            await this.friendshipModel.deleteOne(id);
        } catch (error) {
            throw error;
        }
    }

    async findAllAndCountFriendshipByQuery(query: GetFriendshipListQuery) {
        try {
            const {
                keyword = '',
                page = +DEFAULT_FIRST_PAGE,
                limit = +DEFAULT_LIMIT_FOR_PAGINATION,
                orderBy = DEFAULT_ORDER_BY,
                orderDirection = DEFAULT_ORDER_DIRECTION,
            } = query;

            const matchQuery: FilterQuery<Friendship> = {};
            matchQuery.$and = [
                {
                    ...softDeleteCondition,
                },
            ];

            if (keyword) {
                const keywordRegex = new RegExp(`.*${keyword}.*`, 'i');
                matchQuery.$and.push({
                    $or: [
                        { user_id_send: { $regex: keywordRegex } },
                        { user_id_receive: { $regex: keywordRegex } },
                        { status: { $regex: keywordRegex } },
                    ]
                });
            }

            const [result] = await this.friendshipModel.aggregate([
                {
                    $addFields: {
                        id: { $toString: '$_id' },
                    },
                },
                {
                    $match: {
                        ...matchQuery,
                    },
                },
                {
                    $project: parseMongoProjection(FriendshipAttributesForList),
                },
                {
                    $facet: {
                        count: [{ $count: 'total' }],
                        data: [
                            {
                                $sort: {
                                    [orderBy]:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                    ['_id']:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                },
                            },
                            {
                                $skip: (page - 1) * limit,
                            },
                            {
                                $limit: Number(limit),
                            },
                        ],
                    },
                },
            ]);
            return {
                totalItems: result?.count?.[0]?.total || 0,
                items: result?.data || [],
            };
        } catch (error) {
            this.logger.error(
                'Error in FriendshipRepository findAllAndCountFriendshipByQuery: ' + error,
            );
            throw error;
        }
    }
}
