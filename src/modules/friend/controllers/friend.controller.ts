import { FriendService } from '../services/friend.service';
import { CreateFriendshipDto, GetFriendshipListQuery, UpdateFriendshipDto } from '../friend.interface';
import {
    Controller,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    Get,
    Query,
    HttpException,
} from '@nestjs/common';
import { ErrorResponse, SuccessResponse } from '../../../common/helpers/response';
import { HttpStatus, mongoIdSchema } from '../../../common/constants';
import {
    ApiResponseError,
    SwaggerApiType,
    ApiResponseSuccess,
} from '../../../common/services/swagger.service';
import { ApiOperation, ApiBody, ApiTags } from '@nestjs/swagger';

import { TrimBodyPipe } from '../../../common/pipe/trim.body.pipe';
import { toObjectId } from '../../../common/helpers/commonFunctions';
import { BaseController } from '../../../common/base/base.controller';
import { JoiValidationPipe } from '../../../common/pipe/joi.validation.pipe';
import { createFriendshipSuccessResponseExample, deleteFriendshipSuccessResponseExample, getFriendshipDetailSuccessResponseExample, getFriendshipListSuccessResponseExample } from '../friend.swagger';

@Controller('friend')
export class FriendController extends BaseController {
    constructor(
        private readonly friendshipService: FriendService,
    ) {
        super();
    }

    @ApiOperation({ summary: 'Create Friendship' })
    @ApiResponseError([SwaggerApiType.CREATE])
    @ApiResponseSuccess(createFriendshipSuccessResponseExample)
    @ApiBody({ type: CreateFriendshipDto })
    @Post()
    async createFriendship(
        @Body() dto: CreateFriendshipDto,
    ) {
        try {
            const result = await this.friendshipService.createFriendship(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @ApiOperation({ summary: 'Get Friendship detail by id' })
    @ApiResponseError([SwaggerApiType.GET_DETAIL])
    @ApiResponseSuccess(getFriendshipDetailSuccessResponseExample)
    @Get(':id')
    async getFriendshipDetail(
        @Param('id', new JoiValidationPipe(mongoIdSchema))
        id: string,
    ) {
        try {
            const result = await this.friendshipService.getFriendshipById(toObjectId(id));
            return new SuccessResponse(result);
        } catch (err) {
            this.handleError(err);
        }
    }

    @Post('get-friendship-list-by-userid')
    async getFriendshipListByUserId(
        @Body() userId: { userId: string }
    ) {
        try {
            const friendships = await this.friendshipService.getFriendshipListByUserId(userId.userId);
            return new SuccessResponse(friendships);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('unfriend')
    async unfriend(
        @Body() dto: { user_id_1: string, user_id_2: string }
    ) {
        try {
            const result = await this.friendshipService.unfriend(dto.user_id_1, dto.user_id_2);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Patch(':id')
    async updateFriendship(
        @Param('id', new JoiValidationPipe(mongoIdSchema))
        id: string,
        @Body() dto: UpdateFriendshipDto
    ) {
        try {
            const result = await this.friendshipService.updateFriendship(toObjectId(id), dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('accept')
    async doneFriendship(
        @Body() dto: { user_id_send: string, user_id_receive: string }
    ) {
        try {
            const result = await this.friendshipService.acceptFriendship(dto);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Delete(':id')
    async deleteFriendship(
        @Param('id', new JoiValidationPipe(mongoIdSchema))
        id: string,
    ) {
        try {
            const friendship = await this.friendshipService.findFriendshipById(toObjectId(id));

            if (!friendship) {
                return new ErrorResponse(
                    HttpStatus.ITEM_NOT_FOUND,
                    'No friendship'
                );
            }

            const result = await this.friendshipService.deleteFriendship(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('friendship-suggestions')
    async getFriendshipSuggestions(
        @Body() dto: { userId: string }
    ) {
        try {
            if (dto.userId === "") {
                throw new HttpException('Không tìm thấy người dùng này', HttpStatus.BAD_REQUEST);
            }
            
            const friendships = await this.friendshipService.getFriendshipSuggestions(dto.userId);
            return new SuccessResponse(friendships);
        } catch (error) {
            this.handleError(error);
        }
    }
}
