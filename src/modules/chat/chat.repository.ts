import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Chat, ChatDocument } from '../../database/schemas/chat.schema';
import { BaseRepository } from '../../common/base/base.repository';
import { UserRepository } from '../user/user.repository';
import { UserAttributesForDetail } from '../user/user.constant';
import { User } from '../../database/schemas/user.schema';

@Injectable()
export class ChatRepository extends BaseRepository<Chat> {
    constructor(
        @InjectModel(Chat.name)
        private readonly chatModel: Model<ChatDocument>,
        private readonly userRepository: UserRepository,
    ) {
        super(chatModel);
    }

    async create(chat: Partial<Chat>): Promise<Chat> {
        const createdChat = new this.chatModel(chat);
        return createdChat.save();
    }

    async delete(chatId: string): Promise<Chat> {
        return this.chatModel.findByIdAndDelete(chatId).exec();
    }

    async findConversationsByUser(
        userId: string,
        attributes: (keyof User)[] = UserAttributesForDetail,
    ): Promise<any> {
        try {
            const conversations = await this.chatModel.find({
                $or: [
                    { user_id_send: userId },
                    { user_id_receive: userId }
                ]
            }).exec();
            const conversationsWithMessages: { userId: string, user: any, messages: any[] }[] = [];
            for (const conversation of conversations) {
                const counterpartId = conversation.user_id_send === userId ? conversation.user_id_receive : conversation.user_id_send;
                const user = await this.userRepository.getOneById(counterpartId, attributes);
    
                const messageType = conversation.user_id_send === userId ? 'sent' : 'received';
    
                const messageWithMetadata = {
                    ...conversation.toObject(),
                    messageType: messageType
                };
    
                let conversationIndex = conversationsWithMessages.findIndex(con => con.userId === user.id);
                if (conversationIndex === -1) {
                    conversationsWithMessages.push({ userId: user.id, user: user, messages: [messageWithMetadata] });
                } else {
                    conversationsWithMessages[conversationIndex].messages.push(messageWithMetadata);
                }
            }
            return {
                totalItems: conversationsWithMessages.length || 0,
                items: conversationsWithMessages || [],
            };
        } catch (error) {
            this.logger.error('Error in ChatRepository findConversationsByUser:' + error);
            throw error;
        }
    }
    
}
