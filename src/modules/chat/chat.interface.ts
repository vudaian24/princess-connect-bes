import { Document } from 'mongoose';

export interface ChatMessage extends Document {
    message: string;
}
