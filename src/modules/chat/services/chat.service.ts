import { Injectable } from '@nestjs/common';
import { ChatRepository } from './../chat.repository'; // Import ChatRepository để tương tác với cơ sở dữ liệu
import { BaseService } from '../../../common/base/base.service'; // Import BaseService nếu có
import { Chat } from '../../../database/schemas/chat.schema'; // Import Chat schema
import { UserRepository } from '../../../modules/user/user.repository';
import { SuccessResponse } from '../../../common/helpers/response';

@Injectable()
export class ChatService extends BaseService<Chat, ChatRepository> {
    constructor(
        private readonly chatRepository: ChatRepository,
        private readonly userRepository: UserRepository,
    ) {
        super(chatRepository);
    }
    async create(chat: Partial<Chat>): Promise<Chat> {
        return this.chatRepository.create(chat);
    }

    async delete(chatId: string): Promise<Chat> {
        return this.chatRepository.delete(chatId);
    }

    async findByUserId(userId: string): Promise<any> {
        const chat = await this.chatRepository.findConversationsByUser(userId);
        return new SuccessResponse(chat);;
    }
}
