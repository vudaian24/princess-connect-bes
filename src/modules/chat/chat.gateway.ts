import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ChatService } from './services/chat.service';
import { Chat } from '../../database/schemas/chat.schema';

@WebSocketGateway(8001, { cors: { origin: '*' } })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  constructor(private readonly chatService: ChatService) { }

  afterInit(server: Server) {
    console.log('WebSocket Gateway initialized');
  }

  handleConnection(client: Socket, ...args: any[]) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('sendMessage')
  async handleMessage(client: Socket, payload: { user_id_send: string; user_id_receive: string; content: string }): Promise<void> {
    const createdChat = await this.chatService.create({
      user_id_send: payload.user_id_send,
      user_id_receive: payload.user_id_receive,
      content: payload.content
    });
    this.server.emit('receiveMessage', createdChat);
  }

  @SubscribeMessage('getChatsByUserId')
  async getChatsByUserId(client: Socket, payload: { user_id: string }): Promise<void> {
    const chats = await this.chatService.findByUserId(payload.user_id);
    client.emit(`${payload.user_id}_chats`, chats);
  }
}