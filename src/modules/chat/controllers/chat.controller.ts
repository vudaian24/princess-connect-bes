import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ChatService } from './../services/chat.service';
import { Chat } from '../../../database/schemas/chat.schema';
import { SuccessResponse } from '../../../common/helpers/response';
import { BaseController } from '../../../common/base/base.controller';

@Controller('chat')
export class ChatController extends BaseController {
    constructor(
        private readonly chatService: ChatService
    ) { 
        super();
    }

    @Get('user/:id')
    async getChatsByUserId(
        @Param('id') id: string
    ) {
        const res = await this.chatService.findByUserId(id);
        return res;
    }

    @Post()
    async createChat(
        @Body() payload: { user_id_send: string; user_id_receive: string; content: string }
    ) {
        const createdChat = await this.chatService.create({
            user_id_send: payload.user_id_send,
            user_id_receive: payload.user_id_receive,
            content: payload.content
        });
        return new SuccessResponse(createdChat);
    }
}
