import { PostService } from '../services/post.service';
import { CreatePostDto, GetPostListQuery } from '../post.interface';
import {
    Controller,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    Get,
    Query,
    UseGuards,
    UseInterceptors,
    UploadedFile,
} from '@nestjs/common';
import { ErrorResponse, SuccessResponse } from '../../../common/helpers/response';
import { HttpStatus, mongoIdSchema } from '../../../common/constants';
import {
    ApiResponseError,
    SwaggerApiType,
    ApiResponseSuccess,
} from '../../../common/services/swagger.service';
import { ApiOperation, ApiBody, ApiTags } from '@nestjs/swagger';

import { TrimBodyPipe } from '../../../common/pipe/trim.body.pipe';
import { toObjectId } from '../../../common/helpers/commonFunctions';
import { BaseController } from '../../../common/base/base.controller';
import { JoiValidationPipe } from '../../../common/pipe/joi.validation.pipe';
import { createPostSuccessResponseExample, deletePostSuccessResponseExample, getPostDetailSuccessResponseExample, getPostListSuccessResponseExample } from '../post.swagger';
import { AuthGuard } from '../../../modules/auth/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
@Controller('post')
export class PostController extends BaseController {
    constructor(
        private readonly postService: PostService,
    ) {
        super();
    }

    @ApiOperation({ summary: 'Create Post' })
    @ApiResponseError([SwaggerApiType.CREATE])
    @ApiResponseSuccess(createPostSuccessResponseExample)
    @ApiBody({ type: CreatePostDto })
    @Post()
    @UseInterceptors(FileInterceptor('image', {}))
    async createPost(
        @Body() dto: CreatePostDto,
        @UploadedFile() image: Express.Multer.File
    ) {
        try {
            const result = await this.postService.createPost(dto, image);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @ApiOperation({ summary: 'Get Post detail by id' })
    @ApiResponseError([SwaggerApiType.GET_DETAIL])
    @ApiResponseSuccess(getPostDetailSuccessResponseExample)
    @Get(':id')
    async getPostDetail(
        @Param('id', new JoiValidationPipe(mongoIdSchema)) 
        id: string,
    ) {
        try {
            const result = await this.postService.getPostById(toObjectId(id));
            if(!result) {
                return new ErrorResponse(
                    HttpStatus.ITEM_NOT_FOUND,
                    'No post'
                );
            }
            return new SuccessResponse(result);
        } catch(err) {
            this.handleError(err);
        }
    }

    @Post('get-post-list-by-userid')
    async getPostListByUserId(
        @Body() userId: {userId: string} 
    ) {
        try {
            const posts = await this.postService.getPostListByUserId(userId);
            return new SuccessResponse(posts);
        } catch (error) {
            this.handleError(error);
        }
    }
    @Patch('update-like')
    async updateLike(
        @Body() dto: {postId: string, userLike: string}
    ) {
        try {
            const result = await this.postService.updateLike(dto.postId, dto.userLike);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
    
    @Post('add-comment')
    async addComment(
        @Body() dto: {postId: string, userId: string, content: string}
    ) {
        try {
            const result = await this.postService.addComment(dto.userId, dto.postId, dto.content);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('get-all-comments')
    async getAllCommentsInPost(
        @Body() body: {postId: string}
    ) {
        try {
            const result = await this.postService.getComments(body.postId);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }


    // @UseGuards(AuthGuard)
    @ApiOperation({ summary: 'Get Post list' })
    @ApiResponseError([SwaggerApiType.GET_LIST])
    @ApiResponseSuccess(getPostListSuccessResponseExample)
    @Get()
    async getPostList(
        @Query(new JoiValidationPipe())
        query: GetPostListQuery,
    ) {
        try {
            const result = await this.postService.findAllAndCountPostByQuery(query);
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }

    @ApiOperation({ summary: 'Delete User by id' })
    @ApiResponseError([SwaggerApiType.DELETE])
    @ApiResponseSuccess(deletePostSuccessResponseExample)
    @Delete(':id')
    async deleteUser(
        @Param('id', new JoiValidationPipe(mongoIdSchema)) 
        id: string,
    ) {
        try {
            const post = await this.postService.findPostById(toObjectId(id));

            if (!post) {
                return new ErrorResponse(
                    HttpStatus.ITEM_NOT_FOUND,
                    'No post'
                );
            }

            const result = await this.postService.deletePost(toObjectId(id));
            return new SuccessResponse(result);
        } catch (error) {
            this.handleError(error);
        }
    }
}
