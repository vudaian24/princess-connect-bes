import { BaseRepository } from '../../common/base/base.repository';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { GetPostListQuery } from './post.interface';
import {
    DEFAULT_FIRST_PAGE,
    DEFAULT_LIMIT_FOR_PAGINATION,
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIRECTION,
    OrderDirection,
    softDeleteCondition,
} from '../../common/constants';
import { parseMongoProjection } from '../../common/helpers/commonFunctions';
import { Post, PostDocument } from '../../database/schemas/post.schema';
import { PostAttributesForList } from './post.constant';

@Injectable()
export class PostRepository extends BaseRepository<Post> {
    constructor(
        @InjectModel(Post.name)
        private readonly postModel: Model<PostDocument>,
    ) {
        super(postModel);
    }

    async getTotalPost(): Promise<any> {
        try {
            const postCount = await this.postModel.countDocuments({
                deletedAt: null
            });
            return postCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalAll: ' + error);
            throw error;
        }
    }

    async getTotalPostDeleted(): Promise<any> {
        try {
            const postCount = await this.postModel.countDocuments({
                deletedAt: { $ne: null }
            });
            return postCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalAll: ' + error);
            throw error;
        }
    }

    async getTotalPostToday(): Promise<any> {
        try {
            const today = new Date();
            today.setHours(0, 0, 0, 0);
            const tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            tomorrow.setHours(0, 0, 0, 0);
            const totalPost = await this.postModel.countDocuments({
                createdAt: {
                    $gte: today,
                    $lt: tomorrow,
                },
                deletedAt: null
            });
            return totalPost;
        } catch (error) {
            this.logger.error('Error in PostRepository getTotalPostToday: ' + error);
            throw error;
        }
    }
    async getTotalPostMonth(): Promise<any> {
        try {
            const today = new Date();
            const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
            const endOfDay = new Date(today.setHours(23, 59, 59, 999));

            const postCount = await this.postModel.countDocuments({
                createdAt: { $gte: startOfMonth, $lte: endOfDay },
                deletedAt: null
            });
            return postCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalUserMonth: ' + error);
            throw error;
        }
    }

    async findOneBy(condition: Partial<Post>): Promise<Post | null> {
        try {
            const post = await this.postModel.findOne(condition);
            return post || null;
        } catch (error) {
            this.logger.error('Error in PostRepository findOneBy: ' + error);
            throw error;
        }
    }

    async findAllByQuery(condition: Partial<Post>) {
        try {
            const posts = await this.postModel.find(condition); 
            return {
                totalItems: posts.length,
                items: posts || []
            };
        } catch (error) {
            this.logger.error('Error in PostRepository find: ' + error);
            throw error;
        }
    }

    async findAllAndCountPostByQuery(query: GetPostListQuery) {
        try {
            const {
                keyword = '',
                page = +DEFAULT_FIRST_PAGE,
                limit = +DEFAULT_LIMIT_FOR_PAGINATION,
                orderBy = DEFAULT_ORDER_BY,
                orderDirection = DEFAULT_ORDER_DIRECTION,
            } = query;

            const matchQuery: FilterQuery<Post> = {};
            matchQuery.$and = [
                {
                    ...softDeleteCondition,
                },
            ];

            if (keyword) {
                const keywordRegex = new RegExp(`.*${keyword}.*`, 'i');
                matchQuery.$and.push({
                    $or: [
                        { content: { $regex: keywordRegex } },
                    ]
                });
            }

            const [result] = await this.postModel.aggregate([
                {
                    $addFields: {
                        id: { $toString: '$_id' },
                    },
                },
                {
                    $match: {
                        ...matchQuery,
                    },
                },
                {
                    $project: parseMongoProjection(PostAttributesForList),
                },
                {
                    $facet: {
                        count: [{ $count: 'total' }],
                        data: [
                            {
                                $sort: {
                                    [orderBy]:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                    ['_id']:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                },
                            },
                            {
                                $skip: (page - 1) * limit,
                            },
                            {
                                $limit: Number(limit),
                            },
                        ],
                    },
                },
            ]);
            return {
                totalItems: result?.count?.[0]?.total || 0,
                items: result?.data || [],
            };
        } catch (error) {
            this.logger.error(
                'Error in PostRepository findAllAndCountPostByQuery: ' + error,
            );
            throw error;
        }
    }
}