export const createPostSuccessResponseExample = {
    createdAt: '2024-01-10T10:46:42.037Z',
    updatedAt: '2024-01-10T10:46:42.037Z',
    deletedAt: null,
    deletedBy: null,
    updatedBy: null,
    createdBy: null,
    user_id: 'Post name 1',
    _id: '659e7592b3b56d0946b3c7b5',
    __v: 0,
    id: '659e7592b3b56d0946b3c7b5',
};

export const updatePostSuccessResponseExample = {
    _id: '659e7592b3b56d0946b3c7b5',
    user_id: 'new user_id',
    id: '659e7592b3b56d0946b3c7b5',
};

export const deletePostSuccessResponseExample = {
    id: '659e7592b3b56d0946b3c7b5',
};

export const getPostDetailSuccessResponseExample = {
    _id: '659e7592b3b56d0946b3c7b5',
    user_id: 'new user_id',
    id: '659e7592b3b56d0946b3c7b5',
};

export const getPostListSuccessResponseExample = {
    totalItems: 1,
    items: [
        {
            _id: '659e7592b3b56d0946b3c7b5',
            createdAt: '2024-01-10T10:46:42.037Z',
            updatedAt: '2024-01-10T10:47:59.566Z',
            id: '659e7592b3b56d0946b3c7b5',
        },
    ],
};