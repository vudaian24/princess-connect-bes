import { Post } from '../../database/schemas/post.schema';

export enum PostOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updatedAt',
}

export const PostAttributesForList: (keyof Post)[] = [
    '_id',
    'id',
    'user_id',
    'image',
    'content',
    'like',
    'comment',
    'createdAt',
    'updatedAt',
];

export const PostAttributesForDetail: (keyof Post)[] = [
    'id',
    'user_id',
    'image',
    'content',
    'like',
    'comment',
    'createdAt',
];
