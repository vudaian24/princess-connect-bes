import { Module } from '@nestjs/common';
import { PostController } from './controllers/post.controller';
import { PostService } from './services/post.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Post, PostSchema } from '../../database/schemas/post.schema';
import { JwtModule } from '@nestjs/jwt';
import { PostRepository } from './post.repository';
import { CloudinaryService } from '../../common/services/cloudinary.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CommentModule } from '../comment/comment.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('SECRET'),
        signOptions: { expiresIn: configService.get<string>('EXP_IN_ACCESS_TOKEN') }
      }),
      inject: [ConfigService]
    }),
    CommentModule
  ],
  controllers: [PostController],
  providers: [PostService, PostRepository, CloudinaryService],
  exports: [PostRepository],
})
export class PostModule { }