import { BaseService } from '../../../common/base/base.service';
import { HttpException, Injectable } from '@nestjs/common';
import { Types } from 'mongoose';
import {
    CreatePostDto,
    UpdatePostDto,
    GetPostListQuery,
} from './../post.interface';
import { Post, PostDocument } from '../../../database/schemas/post.schema';
import { PostRepository } from '../post.repository';
import { HttpStatus } from '../../../common/constants';
import { PostAttributesForDetail } from '../post.constant';
import { UserRepository } from '../../../modules/user/user.repository';
import { CloudinaryService } from '../../../common/services/cloudinary.service';
import { toObjectId } from '../../../common/helpers/commonFunctions';
import { Comment } from '../../../database/schemas/comment.schema';
import { CommentRepository } from '../../../modules/comment/comment.repository';
import { UserAttributesForDetail } from '../../../modules/user/user.constant';
import { User } from '../../../database/schemas/user.schema';

@Injectable()
export class PostService extends BaseService<Post, PostRepository> {
    constructor(
        private readonly postRepository: PostRepository,
        private readonly userRepository: UserRepository,
        private readonly commentRepository: CommentRepository,
        private readonly cloudinaryService: CloudinaryService,
    ) {
        super(postRepository);
    }

    async getPostListByUserId(userId: { userId: string }) {
        try {
            const posts = await this.postRepository.findAllByQuery({
                user_id: userId.userId,
                deletedAt: null
            });
            return posts;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async addComment(
        userId: string,
        postId: string,
        content: string,
    ) {
        try {
            const comment: SchemaCreateDocument<Comment> = {
                user_id: userId,
                content: content
            }
            const createdComment = await this.commentRepository.createOneComment(userId, content);
            const posts = await this.postRepository.findAllByQuery({
                _id: toObjectId(postId),
                deletedAt: null
            });
            const post = posts.items[0];
            post.comment.push(createdComment._id.toString());
            await post.save();
            return post;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getComments(
        id: string,
        attributes: (keyof User)[] = UserAttributesForDetail
    ) {
        try {
            const posts = await this.postRepository.findAllByQuery({
                _id: toObjectId(id),
                deletedAt: null
            });
            const post = posts.items[0];
            const commentPromises = post.comment.map(commentId =>
                this.commentRepository.findAllByQuery({
                    _id: toObjectId(commentId),
                    deletedAt: null
                })
            );
            const commentsGroup = await Promise.all(commentPromises);

            const userPromises = [];
            const userIds = [];
            commentsGroup.forEach(commentGroup => {
                commentGroup.items.forEach(comment => {
                    userPromises.push(
                        this.userRepository.getOneById(toObjectId(comment.user_id), attributes)
                    );
                });
            });
            const commnetId: any[] = [];
            const commentContent: any[] = [];
            const comments: any[] = [];
            const users = await Promise.all(userPromises);
            commentsGroup.map((comment) => {
                comment.items.map((comment) => {
                    commnetId.push(users.find(user => user._id.toString() === comment.user_id.toString()));
                    commentContent.push(comment.content);
                })
            })
            commnetId.map((itemId, indexId) => {
                commentContent.map((itemContent, indexContent) => {
                    if (indexId == indexContent) {
                        comments.push({
                            user: itemId,
                            content: itemContent
                        })
                    }
                })
            })
            return {
                totalItems: comments.length,
                items: comments || []
            };
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    async updateLike(postId: string, userLike: string) {
        try {
            const posts = await this.postRepository.findAllByQuery({
                _id: toObjectId(postId),
                deletedAt: null
            });
            const post = posts.items[0];
            const isLiked = post.like.includes(userLike);
            if (isLiked) {
                post.like = post.like.filter(userId => userId.toString() !== userLike.toString());
            } else {
                post.like.push(userLike);
            }
            await post.save();
            return post;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async createPost(dto: CreatePostDto, image: Express.Multer.File) {
        try {
            let imageUrl: string | null = null;
            if (image) {
                imageUrl = await this.cloudinaryService.uploadImage(image, 'post-images');
            }
            const post: SchemaCreateDocument<Post> = {
                user_id: dto.user_id,
                content: dto.content,
                image: imageUrl,
                like: dto.like,
                comment: dto.comment,
            }

            const createdPost = await this.postRepository.createOne(post);

            const updateUser = await this.userRepository.updateOneById(
                dto.user_id,
                { $push: { posts: createdPost._id } }
            );

            return createdPost;
        } catch (error) {
            this.logger.error('Error in PostService createPost: ' + error);
            throw error;
        }
    }

    async findPostById(
        id: Types.ObjectId,
        attributes: (keyof Post)[] = PostAttributesForDetail
    ) {
        try {
            const res = await this.postRepository.getOneById(id, attributes);
            return res;
        } catch (error) {
            this.logger.error('Error in PostService findPostById: ' + error);
            throw error;
        }
    }

    async getPostById(
        id: Types.ObjectId,
        attributes: (keyof Post)[] = PostAttributesForDetail
    ) {
        try {
            const res = await this.postRepository.getOneById(id, attributes);
            const userId = res.user_id;
            const user = await this.userRepository.findOneBy({
                _id: toObjectId(userId),
            });
            return {
                items: {
                    id: res.id,
                    createdAt: res.createdAt,
                    user_id: res.user_id,
                    user: {
                        username: user.username,
                        avatar: user.avatar
                    },
                    content: res.content,
                    image: res.image,
                    like: res.like,
                    comment: res.comment,
                }
            }
        } catch (error) {
            this.logger.error('Error in PostService getPostById: ' + error);
            throw error;
        }
    }

    async deletePost(id: Types.ObjectId) {
        try {
            await this.postRepository.softDeleteOne({ _id: id });
            return { id };
        } catch (error) {
            this.logger.error('Error in PostService deletePost: ' + error);
            throw error;
        }
    }

    async findAllAndCountPostByQuery(query: GetPostListQuery) {
        try {
            const result = await this.postRepository.findAllAndCountPostByQuery(query);

            const userIds = result.items.map(item => item.user_id);

            const userPromises = userIds.map(
                userId => this.userRepository.findOneBy({ _id: userId })
            );

            const users = await Promise.all(userPromises);

            const limitedUsers = users.map(user => ({
                username: user.username,
                avatar: user.avatar
            }));
            result.items.forEach((item, index) => {
                item.user = limitedUsers[index];
            });

            return result;
        } catch (error) {
            this.logger.error(
                'Error in PostService findAllAndCountPostByQuery: ' + error,
            );
            throw error;
        }
    }
}
