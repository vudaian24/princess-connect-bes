import { INPUT_TEXT_MAX_LENGTH, URL_MAX_LENGTH } from '../../common/constants';
import { JoiValidate } from '../../common/decorators/validator.decorator';
import { PostOrderBy } from './post.constant';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import Joi from '../../plugins/joi';
import { CommonListQuery } from '../../common/interfaces';

export class CreatePostDto {
    user_id: string;

    content: string;

    like: string[];
    
    comment: string[];
}

export class UpdatePostDto {
    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'user_id',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH).required())
    user_id: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'Post content',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH).required())
    content: string;

    @ApiProperty({
        type: 'string',
        format: 'binary',
        description: 'User avatar',
        required: true,
    })
    image: Express.Multer.File;

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    like?: string[];

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    comment?: string[];
}

export class GetPostListQuery extends CommonListQuery {
    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'user_id',
    })
    @JoiValidate(Joi.string().valid(...Object.values(PostOrderBy)).optional())
    user_id?: string;
}