import { INPUT_PHONE_MAX_LENGTH, INPUT_TEXT_MAX_LENGTH, URL_MAX_LENGTH } from '../../common/constants';
import { JoiValidate } from '../../common/decorators/validator.decorator';
import { UserOrderBy } from './user.constant';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import Joi from '../../plugins/joi';
import { CommonListQuery } from '../../common/interfaces';

export class CreateUserDto {
    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'user@example.com',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH).required())
    email: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: '',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    password?: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User name',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH).required())
    username: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User birthday',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    birthday?: string;

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    friends?: string[];

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    posts?: string[];

    @ApiProperty({
        type: 'string',
        format: 'binary',
        description: 'User avatar',
        required: true,
    })
    avatar: Express.Multer.File;

    @ApiProperty({
        type: 'string',
        format: 'binary',
        description: 'User cover avatar',
        required: true,
    })
    coverAvatar: Express.Multer.File;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User role',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    role?: string;
}

export class UpdateUserDto {
    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'user@example.com',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    email?: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: '',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    password?: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User name',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    username?: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User birthday',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    birthday?: string;

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    friends?: string[];

    @ApiProperty({
        type: [String],
        default: [],
    })
    @JoiValidate(Joi.array().items(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH)))
    posts?: string[];

    @ApiProperty({
        type: 'string',
        format: 'binary',
        description: 'User avatar',
        required: true,
    })
    avatar: Express.Multer.File;

    @ApiProperty({
        type: 'string',
        format: 'binary',
        description: 'User cover avatar',
        required: true,
    })
    coverAvatar: Express.Multer.File;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: 'User role',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    role?: string;
}

export class GetUserListQuery extends CommonListQuery {
    @ApiPropertyOptional({
        enum: UserOrderBy,
        description: 'Which field used to sort',
        default: UserOrderBy.UPDATED_AT,
    })
    @JoiValidate(
        Joi.string()
            .valid(...Object.values(UserOrderBy))
            .optional(),
    )
    orderBy?: UserOrderBy;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: "User'name for filter",
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH).optional())
    username?: string;

    role: string;
}
