import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { UserRepository } from './user.repository';
import { User, UserSchema } from '../../database/schemas/user.schema';
import { JwtModule } from '@nestjs/jwt';
import { CloudinaryService } from '../../common/services/cloudinary.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get<string>('SECRET'),
                signOptions: { expiresIn: configService.get<string>('EXP_IN_ACCESS_TOKEN') }
            }),
            inject: [ConfigService]
        }),
    ],
    controllers: [UserController],
    providers: [UserService, UserRepository, CloudinaryService],
    exports: [UserRepository],
})
export class UserModule { }