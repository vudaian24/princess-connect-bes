import { BaseRepository } from '../../common/base/base.repository';
import { User, UserDocument } from '../../database/schemas/user.schema';
import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { GetUserListQuery } from './user.interface';
import {
    DEFAULT_FIRST_PAGE,
    DEFAULT_LIMIT_FOR_PAGINATION,
    DEFAULT_ORDER_BY,
    DEFAULT_ORDER_DIRECTION,
    OrderDirection,
    softDeleteCondition,
} from '../../common/constants';
import { parseMongoProjection } from '../../common/helpers/commonFunctions';
import { UserAttributesForList } from './user.constant';

@Injectable()
export class UserRepository extends BaseRepository<User> {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>,
    ) {
        super(userModel);
    }

    async getTotalUser(): Promise<any> {
        try {
            const userCount = await this.userModel.countDocuments({
                role: { $ne: 'admin' },
                deletedAt: null
            });
            return userCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalAll: ' + error);
            throw error;
        }
    }

    async getTotalUserToday(): Promise<any> {
        try {
            const today = new Date();
            const startOfDay = new Date(today.setHours(0, 0, 0, 0));
            const endOfDay = new Date(today.setHours(23, 59, 59, 999));
            const userCount = await this.userModel.countDocuments({
                role: { $ne: 'admin' },
                createdAt: { $gte: startOfDay, $lte: endOfDay },
                deletedAt: null
            });
            return userCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalAll: ' + error);
            throw error;
        }
    }

    async getTotalUserMonth(): Promise<number> {
        try {
            const today = new Date();
            const startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
            const endOfDay = new Date(today.setHours(23, 59, 59, 999));

            const userCount = await this.userModel.countDocuments({
                role: { $ne: 'admin' },
                createdAt: { $gte: startOfMonth, $lte: endOfDay },
                deletedAt: null
            });
            return userCount || 0;
        } catch (error) {
            this.logger.error('Error in UserRepository getTotalUserMonth: ' + error);
            throw error;
        }
    }

    async findOneBy(condition: Partial<User>): Promise<User | null> {
        try {
            const user = await this.userModel.findOne(condition);
            return user || null;
        } catch (error) {
            this.logger.error('Error in UserRepository findOneBy: ' + error);
            throw error;
        }
    }

    async updateRefreshToken(
        email: string,
        refresh_token: string,
    ): Promise<void> {
        try {
            await this.userModel.updateOne({ email }, { refresh_token });
        } catch (error) {
            this.logger.error(
                'Error in UserRepository updateRefreshToken: ' + error,
            );
            throw error;
        }
    }

    async comparePassword(user, password): Promise<boolean> {
        const isMatch = await bcrypt.compare(password, user.password);
        return isMatch;
    }

    async findAllAndCountUserByQuery(query: GetUserListQuery) {
        try {
            const {
                keyword = '',
                page = +DEFAULT_FIRST_PAGE,
                limit = +DEFAULT_LIMIT_FOR_PAGINATION,
                orderBy = DEFAULT_ORDER_BY,
                orderDirection = DEFAULT_ORDER_DIRECTION,
                username = '',
            } = query;

            const matchQuery: FilterQuery<User> = {};
            matchQuery.$and = [
                {
                    ...softDeleteCondition,
                },
            ];

            if (keyword) {
                const keywordRegex = new RegExp(`.*${keyword}.*`, 'i');
                matchQuery.$and.push({
                    $or: [
                        { username: { $regex: keywordRegex } },
                        { email: { $regex: keywordRegex } },
                    ]
                });
            }

            if (username) {
                matchQuery.$and.push({
                    username,
                });
            }

            const [result] = await this.userModel.aggregate([
                {
                    $addFields: {
                        id: { $toString: '$_id' },
                    },
                },
                {
                    $match: {
                        ...matchQuery,
                    },
                },
                {
                    $project: parseMongoProjection(UserAttributesForList),
                },
                {
                    $facet: {
                        count: [{ $count: 'total' }],
                        data: [
                            {
                                $sort: {
                                    [orderBy]:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                    ['_id']:
                                        orderDirection === OrderDirection.ASC
                                            ? 1
                                            : -1,
                                },
                            },
                            {
                                $skip: (page - 1) * limit,
                            },
                            {
                                $limit: Number(limit),
                            },
                        ],
                    },
                },
            ]);
            return {
                totalItems: result?.count?.[0]?.total || 0,
                items: result?.data || [],
            };
        } catch (error) {
            this.logger.error(
                'Error in UserRepository findAllAndCountUserByQuery: ' + error,
            );
            throw error;
        }
    }
}
