import { User } from '../../database/schemas/user.schema';

export enum UserOrderBy {
    ID = 'id',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updatedAt',
}

export const UserAttributesForList: (keyof User)[] = [
    '_id',
    'id',
    'username',
    'email',
    'birthday',
    'avatar',
    'coverAvatar',
    'createdAt',
    'role',
    'updatedAt',
];

export const UserAttributesForDetail: (keyof User)[] = [
    '_id',
    'id',
    'username',
    'email',
    'avatar',
    'friends',
    'birthday',
    'posts',
    'coverAvatar',
    'role',
    'createdAt',
    'updatedAt',
];
