import { BaseService } from '../../../common/base/base.service';
import { HttpException, Injectable } from '@nestjs/common';
import { Types } from 'mongoose';
import {
    CreateUserDto,
    GetUserListQuery,
    UpdateUserDto,
} from '../user.interface';

import { User } from '../../../database/schemas/user.schema';
import { UserRepository } from '../user.repository';
import { UserAttributesForDetail } from '../user.constant';
import { HttpStatus } from '../../../common/constants';
import { hashPassword } from '../../../common/helpers/commonFunctions';
import { CloudinaryService } from '../../../common/services/cloudinary.service';

@Injectable()
export class UserService extends BaseService<User, UserRepository> {
    constructor(
        private readonly userRepository: UserRepository,
        private readonly cloudinaryService: CloudinaryService,
    ) {
        super(userRepository);
    }

    async createUser(dto: CreateUserDto) {
        try {
            const existingUser = await this.userRepository.findOneBy({
                email: dto.email,
                deletedAt: null,
            });

            if (existingUser) {
                throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
            }

            const hashPass = dto.password ? hashPassword(dto.password) : null;

            let avatarUrl: string | null = null;
            if (dto.avatar) {
                avatarUrl = await this.cloudinaryService.uploadImage(dto.avatar, 'avatars');
            }

            let coverAvatarUrl: string | null = null;
            if (dto.coverAvatar) {
                coverAvatarUrl = await this.cloudinaryService.uploadImage(dto.coverAvatar, 'coverAvatars');
            }

            const user: SchemaCreateDocument<User> = {
                username: dto.username,
                email: dto.email,
                password: hashPass,
                birthday: dto.birthday,
                friends: dto.friends || [],
                posts: dto.posts || [],
                avatar: avatarUrl,
                coverAvatar: coverAvatarUrl,
                role: dto.role || 'user'
            };
            return await this.userRepository.createOne(user);
        } catch (error) {
            this.logger.error('Error in UserService createUser: ' + error);
            throw error;
        }
    }

    async updateAvatar(id: Types.ObjectId, avatar: Express.Multer.File) {
        try {
            let avatarUrl: string | null = null;
            if (avatar) {
                avatarUrl = await this.cloudinaryService.uploadImage(avatar, 'avatars');
            }
            const user = {
                avatar: avatarUrl
            };
            await this.userRepository.updateOneById(id, user);
            return await this.findUserById(id);
        } catch (error) {

        }
    }

    async updateCoverAvatar(id: Types.ObjectId, coverAvatar: Express.Multer.File) {
        try {
            let coverAvatarUrl: string | null = null;
            if (coverAvatar) {
                coverAvatarUrl = await this.cloudinaryService.uploadImage(coverAvatar, 'coverAvatars');
            }
            const user = {
                coverAvatar: coverAvatarUrl,
            };
            await this.userRepository.updateOneById(id, user);
            return await this.findUserById(id);
        } catch (error) {

        }
    }

    async updateBirthDay(
        id: Types.ObjectId,
        birthday: string,
    ) {
        try {
            await this.userRepository.updateOneById(id, {
                birthday: birthday,
            });
            return await this.findUserById(id);
        } catch (error) {
            this.logger.error('Error in UserService updateBirthDay: ' + error);
            throw error;
        }
    }

    async updatePassword(
        id: Types.ObjectId,
        newPassword: string,
    ) {
        try {
            const hashPass = newPassword ? hashPassword(newPassword) : null;
            await this.userRepository.updateOneById(id, {
                password: hashPass,
            });
            return await this.findUserById(id);
        } catch (error) {
            this.logger.error('Error in UserService updatePassword: ' + error);
            throw error;
        }
    }

    async updateUser(id: Types.ObjectId, dto: UpdateUserDto) {
        try {
            await this.userRepository.updateOneById(id, dto);
            return await this.findUserById(id);
        } catch (error) {
            this.logger.error('Error in UserService updateUser: ' + error);
            throw error;
        }
    }

    async deleteUser(id: Types.ObjectId) {
        try {
            await this.userRepository.softDeleteOne({ _id: id });
            return { id };
        } catch (error) {
            this.logger.error('Error in UserService deleteUser: ' + error);
            throw error;
        }
    }

    async findUserById(
        id: Types.ObjectId,
        attributes: (keyof User)[] = UserAttributesForDetail,
    ) {
        try {
            return await this.userRepository.getOneById(id, attributes);
        } catch (error) {
            this.logger.error('Error in UserService findUserById: ' + error);
            throw error;
        }
    }

    async findAllAndCountUserByQuery(query: GetUserListQuery) {
        try {
            const result =
                await this.userRepository.findAllAndCountUserByQuery(query);
            return result;
        } catch (error) {
            this.logger.error(
                'Error in UserService findAllAndCountUserByQuery: ' + error,
            );
            throw error;
        }
    }
}
