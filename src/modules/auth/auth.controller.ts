import { Body, Controller, ExecutionContext, Get, Headers, Post, Req, UseGuards } from '@nestjs/common';
import { LoginDto, RegisterDto } from './auth.interface';
import { AuthService } from './auth.service';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { BaseController } from '../../common/base/base.controller';
import { Request } from 'express';
import { SuccessResponse } from '../../common/helpers/response';
import { AuthGuard } from './auth.guard';

@ApiTags('Auth APIs')
@Controller('auth')
export class AuthController extends BaseController {
    constructor(private authService: AuthService) { super(); }


    @ApiBody({ type: LoginDto })
    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        try {
            const res = await this.authService.login(loginDto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Post('register')
    async register(@Body() registerDto: RegisterDto) {
        try {
            const res = await this.authService.register(registerDto);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('refresh-token')
    async refreshToken(@Req() req: Request) {
        try {
            const res = await this.authService.refreshToken(req);
            return new SuccessResponse(res)
        } catch (error) {
            this.handleError(error);
        }
    }

    @Get('get-user-profile')
    async getUser(@Req() req: Request) {
        try {
            const res = await this.authService.getUser(req);
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }

}
