import { JoiValidate } from "../../common/decorators/validator.decorator";
import { INPUT_TEXT_MAX_LENGTH } from "../../common/constants";
import { ApiProperty } from "@nestjs/swagger";
import Joi from "../../plugins/joi";

export class LoginDto {
    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: "abc@example.com",
    })
    @JoiValidate(Joi.string().trim().email().max(INPUT_TEXT_MAX_LENGTH).required())
    email: string;

    @ApiProperty({
        type: String,
        maxLength: INPUT_TEXT_MAX_LENGTH,
        default: '12345',
    })
    @JoiValidate(Joi.string().trim().max(INPUT_TEXT_MAX_LENGTH))
    password: string;
}

export class RegisterDto {
    email?: string;
    password?: string;
    username?: string;
}