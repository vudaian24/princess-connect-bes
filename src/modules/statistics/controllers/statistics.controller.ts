import { Controller, Get } from '@nestjs/common';
import { StatisticsService } from '../services/statistics.service';
import { SuccessResponse } from '../../../common/helpers/response';
import { BaseController } from '../../../common/base/base.controller';

@Controller('statistics')
export class StatisticsController extends BaseController {
    constructor(
        private readonly statisticsService: StatisticsService,
    ) {
        super();
    }
    @Get()
    async getStatistics() {
        try {
            const res = await this.statisticsService.getStatistics();
            return new SuccessResponse(res);
        } catch (error) {
            this.handleError(error);
        }
    }
}
