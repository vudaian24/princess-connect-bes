import { PostRepository } from '../../../modules/post/post.repository';
import { UserRepository } from '../../../modules/user/user.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class StatisticsService {
    constructor(
        private readonly userRepository: UserRepository,
        private readonly postRepository: PostRepository,
    ) { }
    async getStatistics() {
        try {
            const userCount = await this.userRepository.getTotalUser();
            const userCountToday = await this.userRepository.getTotalUserToday();
            const userPercentToday = parseFloat((userCountToday / userCount * 100).toFixed(2));
            const userCountMonth = await this.userRepository.getTotalUserMonth();
            const userPercentMonth = parseFloat((userCountMonth / userCount * 100).toFixed(2));

            const postCount = await this.postRepository.getTotalPost();
            const postCountToday = await this.postRepository.getTotalPostToday();
            const postPercentToday = parseFloat((postCountToday / postCount * 100).toFixed(2));
            const postCountMonth = await this.postRepository.getTotalPostMonth();
            const postPercentMonth = parseFloat((postCountMonth / postCount * 100).toFixed(2));

            const postCountDeleted = await this.postRepository.getTotalPostDeleted();
            const postPercentDeleted = parseFloat((postCountDeleted / postCount * 100).toFixed(2));

            const today = new Date();
            const day = today.getDate();
            const month = today.getMonth() + 1;
            const year = today.getFullYear();
            const date = `${day}-${month}-${year}`;
            return {
                items: {
                    dateTime: date,

                    totalUser: userCount,
                    percentUserToday: userPercentToday,
                    percentUserMonth: userPercentMonth,
                    totalUserToday: userCountToday,
                    totalUserMonth: userCountMonth,

                    totalPost: postCount,
                    percentPostToday: postPercentToday,
                    percentPostMonth: postPercentMonth,
                    totalPostToday: postCountToday,
                    totalPostMonth: postCountMonth,

                    totalPostDeleted: postCountDeleted,
                    percentPostDeleted: postPercentDeleted,
                },
            };

        } catch (error) {
            throw error;
        }
    }
}
