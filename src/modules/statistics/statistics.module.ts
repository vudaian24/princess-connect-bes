import { Module } from '@nestjs/common';
import { StatisticsController } from './controllers/statistics.controller';
import { StatisticsService } from './services/statistics.service';
import { UserService } from '../user/services/user.service';
import { PostService } from '../post/services/post.service';
import { StatisticsRepository } from './statistics.repository';
import { UserModule } from '../user/user.module';
import { PostModule } from '../post/post.module';

@Module({
  imports: [
    UserModule,
    PostModule,
  ],
  controllers: [StatisticsController],
  providers: [StatisticsService, StatisticsRepository]
})
export class StatisticsModule {}
