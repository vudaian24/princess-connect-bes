export enum MongoCollection {
    USERS = 'users',
    POSTS = 'posts',
    CHAT = 'chats',
    COMMENTS = 'comments',
    FRIENDSHIPS = 'friendships',
}