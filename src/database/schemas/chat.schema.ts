import { Prop, Schema } from '@nestjs/mongoose';
import { MongoBaseSchema } from './base.schema';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
export type ChatDocument = SchemaDocument<Chat>;
@Schema({
    timestamps: true,
    collection: MongoCollection.CHAT,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Chat extends MongoBaseSchema {
    @Prop({ required: false })
    user_id_send: string;

    @Prop({ required: false })
    user_id_receive: string;

    @Prop({ required: false })
    content: string;
}

const ChatSchema = createSchemaForClass(Chat);

export { ChatSchema };