import { Prop, Schema } from '@nestjs/mongoose';
import { MongoBaseSchema } from './base.schema';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
export type UserDocument = SchemaDocument<User>;
@Schema({
    timestamps: true,
    collection: MongoCollection.USERS,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})
export class User extends MongoBaseSchema {
    @Prop({ required: false })
    email: string;

    @Prop({ required: false })
    password: string;

    @Prop({ required: false })
    username: string;

    @Prop({ required: false })
    birthday: string;

    @Prop({ type: [String], default: [] })
    friends: string[];

    @Prop({ type: [String], default: [] })
    posts: string[];

    @Prop({ required: false })
    avatar: string;

    @Prop({ required: false })
    coverAvatar: string;

    @Prop({ required: false })
    role: string;
}

const UserSchema = createSchemaForClass(User);

export { UserSchema };
