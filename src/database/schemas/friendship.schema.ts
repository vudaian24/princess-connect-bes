import { Prop, Schema } from '@nestjs/mongoose';
import { MongoBaseSchema } from './base.schema';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
export type FriendshipDocument = SchemaDocument<Friendship>;
@Schema({
    timestamps: true,
    collection: MongoCollection.FRIENDSHIPS,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Friendship extends MongoBaseSchema {
    @Prop({ required: false })
    user_id_send: string;

    @Prop({ required: false })
    user_id_receive: string;

    @Prop({ required: false })
    status: string;
}

const FriendshipSchema = createSchemaForClass(Friendship);

export { FriendshipSchema };