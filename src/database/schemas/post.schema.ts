import { Prop, Schema } from '@nestjs/mongoose';
import { MongoBaseSchema } from './base.schema';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
export type PostDocument = SchemaDocument<Post>;
@Schema({
    timestamps: true,
    collection: MongoCollection.POSTS,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Post extends MongoBaseSchema {
    @Prop({ required: false })
    user_id: string;

    @Prop({ required: false })
    content: string;

    @Prop({ required: false })
    image: string;

    @Prop({ type: [String], default: [] })
    like: string[];

    @Prop({ type: [String], default: [] })
    comment: string[];
}

const PostSchema = createSchemaForClass(Post);

export { PostSchema };