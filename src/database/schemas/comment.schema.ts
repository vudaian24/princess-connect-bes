import { Prop, Schema } from '@nestjs/mongoose';
import { MongoBaseSchema } from './base.schema';
import { MongoCollection } from '../utils/constants';
import { createSchemaForClass } from '../utils/helper';
export type CommentDocument = SchemaDocument<Comment>;
@Schema({
    timestamps: true,
    collection: MongoCollection.COMMENTS,
    toJSON: {
        virtuals: true,
    },
    toObject: {
        virtuals: true,
    },
})

export class Comment extends MongoBaseSchema {
    @Prop({ required: false })
    user_id: string;

    @Prop({ required: false })
    content: string;
}

const CommentSchema = createSchemaForClass(Comment);

export { CommentSchema };